package controllers

import (
	"fmt"
	"net/http"
	"regexp"

	"github.com/gin-gonic/gin"
	"gitlab.com/ayham96/customer_application/application"
	"gitlab.com/ayham96/customer_application/application/applicationservice"
)

type ApplicationController struct {
	applicationSvc *applicationservice.ApplicationService
}

func NewApplicationController(applicationSvc *applicationservice.ApplicationService) *ApplicationController {
	return &ApplicationController{
		applicationSvc: applicationSvc,
	}
}

func (ctrl *ApplicationController) GetByID(c *gin.Context) {
	id := c.Params.ByName("id")
	if !isValidUUID(id) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("invalid uuid: %s", id),
		})
		return
	}

	app, err := ctrl.applicationSvc.GetByID(id)
	if err == applicationservice.ErrNotFound {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"error": fmt.Sprintf("application with id %s not found", id),
		})
		return
	} else if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
		return
	}

	c.JSON(http.StatusOK, app)
}

func (ctrl *ApplicationController) List(c *gin.Context) {
	byStatus := c.Query("status")
	applications, err := ctrl.applicationSvc.ListByStatus(byStatus)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
	}

	c.JSON(http.StatusOK, applications)
}

func (ctrl *ApplicationController) Create(c *gin.Context) {
	var input application.ApplicationInput
	err := c.Bind(&input)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": "invalid application payload",
		})
		return
	}
	if valid, err := isValidApplicationPayload(&input); !valid {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	application, err := ctrl.applicationSvc.Create(&input)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})
		return
	}

	c.JSON(http.StatusCreated, application)
}

func isValidUUID(uuid string) bool {
	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")
	return r.MatchString(uuid)
}

func isValidName(name string) bool {
	r := regexp.MustCompile("^(?i)[a-zäöå]{2,25}$")
	return r.MatchString(name)
}

func isValidApplicationPayload(input *application.ApplicationInput) (bool, error) {
	if !isValidName(input.FirstName) {
		return false, fmt.Errorf("invalid first name: %s", input.FirstName)
	}
	if !isValidName(input.LastName) {
		return false, fmt.Errorf("invalid last name: %s", input.LastName)
	}

	return true, nil
}
