package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ayham96/customer_application/web/controllers"
)

type Router struct {
	engine                *gin.Engine
	applicationController *controllers.ApplicationController
}

func New(applicationController *controllers.ApplicationController) *Router {
	return &Router{
		applicationController: applicationController,
	}
}

var db = make(map[string]string)

func (router *Router) setup() {
	router.engine = gin.Default()

	api := router.engine.Group("/api/v1")

	api.GET("/applications/:id", router.applicationController.GetByID)
	api.GET("/applications", router.applicationController.List)
	api.POST("/applications", router.applicationController.Create)

	api.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	api.GET("/pong", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	// Get user value
	api.GET("/user/:name", func(c *gin.Context) {
		user := c.Params.ByName("name")
		value, ok := db[user]
		if ok {
			c.JSON(http.StatusOK, gin.H{"user": user, "value": value})
		} else {
			c.JSON(http.StatusOK, gin.H{"user": user, "status": "no value"})
		}
	})

	// Authorized group (uses gin.BasicAuth() middleware)
	// Same than:
	// authorized := r.Group("/")
	// authorized.Use(gin.BasicAuth(gin.Credentials{
	//	  "foo":  "bar",
	//	  "manu": "123",
	//}))
	// authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
	// 	"foo":  "bar", // user:foo password:bar
	// 	"manu": "123", // user:manu password:123
	// }))

	// authorized.POST("admin", func(c *gin.Context) {
	// 	user := c.MustGet(gin.AuthUserKey).(string)

	// 	// Parse JSON
	// 	var json struct {
	// 		Value string `json:"value" binding:"required"`
	// 	}

	// 	if c.Bind(&json) == nil {
	// 		db[user] = json.Value
	// 		c.JSON(http.StatusOK, gin.H{"status": "ok"})
	// 	}
	// })
}

func (router *Router) Run(port string) {
	router.setup()
	router.engine.Run(":" + port)
}

func (router *Router) Terminate() {

}
