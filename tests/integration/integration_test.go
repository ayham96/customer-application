// +build integration

package integration

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/ayham96/customer_application/application"
)

const (
	serviceBaseURL                = "http://localhost:8080/api/v1/applications"
	applicationCompletionWaitTime = time.Second * 25
)

func TestCreateApplicationFlow(t *testing.T) {
	// Create Application
	createdApp, err := createApplicationRequest(&application.ApplicationInput{
		FirstName: "Foo",
		LastName:  "Bar",
	})
	if err != nil {
		t.Errorf("failed to create application: %s", err)
	}

	// Get Application by ID
	appByID, err := getApplicationByID(createdApp.ID)
	if err != nil {
		t.Errorf("failed to get created application: %s", err)
	}
	if appByID.ID != createdApp.ID {
		t.Errorf("get application by id returned wrong application")
	}

	// List Applications by status
	pendingApps, err := listApplicationsByStatus(application.ApplicationStatusPending)
	if err != nil {
		t.Errorf("failed to list appliscations by status: %s", err)
	}
	found := false
	for _, app := range pendingApps {
		if app.ID == createdApp.ID {
			found = true
			break
		}
	}
	if !found {
		t.Errorf("created app not found in pending applications")
	}

	// Wait for status update
	deadline := time.Now().Add(applicationCompletionWaitTime)
	currentStatus := appByID.Status
	for time.Now().Before(deadline) {
		app, err := getApplicationByID(createdApp.ID)
		if err != nil {
			t.Errorf("failed to get created application: %s", err)
		}
		if app.Status != currentStatus {
			currentStatus = app.Status
			break
		}
		time.Sleep(time.Second)
	}
	if currentStatus == application.ApplicationStatusPending {
		t.Errorf("application status is still pending")
	}
}

func listApplicationsByStatus(id string) ([]*application.Application, error) {
	resp, err := http.Get(fmt.Sprintf("%s?status=%s", serviceBaseURL, id))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"failed to create application got http status %d expected %d",
			resp.StatusCode,
			http.StatusOK,
		)
	}

	var appsResp []*application.Application
	err = json.Unmarshal(body, &appsResp)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	return appsResp, err

}

func getApplicationByID(id string) (*application.Application, error) {
	resp, err := http.Get(fmt.Sprintf("%s/%s", serviceBaseURL, id))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"failed to create application got http status %d expected %d",
			resp.StatusCode,
			http.StatusOK,
		)
	}

	var appResp application.Application
	err = json.Unmarshal(body, &appResp)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	return &appResp, err
}

func createApplicationRequest(input *application.ApplicationInput) (*application.Application, error) {
	jsonPayload, err := json.Marshal(input)
	req, err := http.NewRequest("POST", serviceBaseURL, bytes.NewBuffer(jsonPayload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	if resp.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("failed to create application got http status %d expected %d", resp.StatusCode, http.StatusCreated)
	}

	var appResp application.Application
	err = json.Unmarshal(body, &appResp)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	return &appResp, err

}
