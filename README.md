# Customer Application

## Dependencies

- make
- docker-compose

## Run instructions

To start all the services please run `make up`.

## Docs

OpenAPI schema can be found in `./docs` or on swagger hub [here](https://app.swaggerhub.com/apis/ayham-alkazaz/customer-application-interview/1.0.0-oas3).
