DB_URL?=postgres://loan_application_user:loan_application_pass@localhost:5433/loan_application?sslmode=disable

up:
	docker-compose up -d

down:
	docker-compose down

migrate_up:
	docker run --rm --network host \
		-v $(CURDIR)/db/migrations:/migrations\
		migrate/migrate \
    	-path=/migrations/ -database $(DB_URL) up

migrate_down:
	docker run --rm --network host \
		-v $(CURDIR)/db/migrations:/migrations\
		migrate/migrate \
    	-path=/migrations/ -database $(DB_URL) down -all

generate_mocks:
	mockgen -source=./application/application.go -destination=./tests/mocks/mock_application/mock_application.go

test_integration:
	go test ./tests/integration/... -tags=integration -count=1

image:
	docker build -t ayhamalkazaz/customer_application:latest .
