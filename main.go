package main

import (
	"fmt"
	"log"

	env "github.com/Netflix/go-env"
	"github.com/streadway/amqp"
	"gitlab.com/ayham96/customer_application/application/applicationclient"
	"gitlab.com/ayham96/customer_application/application/applicationrepo"
	"gitlab.com/ayham96/customer_application/application/applicationservice"
	"gitlab.com/ayham96/customer_application/events/publisher"
	subscriber "gitlab.com/ayham96/customer_application/events/subscriper"
	"gitlab.com/ayham96/customer_application/web/controllers"
	"gitlab.com/ayham96/customer_application/web/router"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Config struct {
	Port string `env:"PORT,default=8080"`

	DBHost string `env:"DB_HOST,default=localhost"`
	DBPort int    `env:"DB_PORT,default=5433"`
	DBUser string `env:"DB_USER,default=loan_application_user"`
	DBPass string `env:"DB_PASS,default=loan_application_pass"`
	DBName string `env:"DB_NAME,default=loan_application"`

	ApplicationsAPIBaseURL string `env:"APPLICATIONS_API_BASE_URL,default=http://localhost:8000/api/applications"`

	AMQPConURL                 string `env:"AMQP_CON_URL,default=amqp://rabbit_user:rabbit_pass@localhost:5673/"`
	ApplicationsExchangeName   string `env:"APPLICATIONS_EXCHANGE_NAME,default=loan_applications_exchange"`
	ApplicationStatusQueueName string `env:"APPLICATION_POLLER_QUEUE_NAME,default=customer_application_status_updates"`
}

func main() {
	config := parseConfig()

	conURL := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.DBHost, config.DBPort, config.DBUser, config.DBPass, config.DBName,
	)
	dbCon, err := sqlx.Connect("postgres", conURL)
	failOnErr(err, "failed to connect to database")
	defer dbCon.Close()

	applicationRepo, err := applicationrepo.New(dbCon)
	failOnErr(err, "failed to create application repository")
	defer applicationRepo.Close()

	amqpCon, err := amqp.Dial(config.AMQPConURL)
	failOnErr(err, "failed to connect to RabbitMQ")
	defer amqpCon.Close()

	publisherCh, err := amqpCon.Channel()
	failOnErr(err, "failed to create amqp channel")
	defer publisherCh.Close()
	pub, err := publisher.New(publisherCh, config.ApplicationsExchangeName)

	applicationSvc := applicationservice.New(applicationclient.New(config.ApplicationsAPIBaseURL), applicationRepo, pub)

	subscriberCh, err := amqpCon.Channel()
	failOnErr(err, "failed to create amqp channel")
	defer publisherCh.Close()
	sub, err := subscriber.New(
		subscriberCh,
		applicationSvc.HandleApplicationStatusUpdated,
		&subscriber.SubscriberConfig{
			ExchangeName:          config.ApplicationsExchangeName,
			QueueName:             config.ApplicationStatusQueueName,
			MaxConcurrentHandlers: 100,
		},
	)
	go func() {
		err := sub.Run()
		failOnErr(err, "failed to run subscriber")
	}()

	applicationController := controllers.NewApplicationController(applicationSvc)

	r := router.New(applicationController)

	r.Run(config.Port)
}

func parseConfig() *Config {
	var config Config
	_, err := env.UnmarshalFromEnviron(&config)
	failOnErr(err, "failed to read config")
	return &config
}

func failOnErr(err error, msg string) {
	if err != nil {
		log.Fatalln(fmt.Sprintf("%s : %v", msg, err))
	}
}
