package events

const (
	RoutingKeyApplicationCreated       = "application.created"
	RoutingKeyApplicationStatusUpdated = "application.status_updated"
)

type ApplicationCreated struct {
	ApplicationID string `json:"application_id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Status        string `json:"status"`
}

type ApplicationStatusUpdated struct {
	ApplicationID string `json:"application_id"`
	Status        string `json:"status"`
}
