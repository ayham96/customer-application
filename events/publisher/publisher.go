package publisher

import (
	"encoding/json"

	"github.com/streadway/amqp"

	events "gitlab.com/ayham96/customer_application/events"
)

type Publisher struct {
	ch           *amqp.Channel
	exchangeName string
}

func New(ch *amqp.Channel, exchangeName string) (*Publisher, error) {
	err := ch.ExchangeDeclare(
		exchangeName, // name
		"topic",      // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)

	if err != nil {
		return nil, err
	}

	return &Publisher{
		ch:           ch,
		exchangeName: exchangeName,
	}, nil
}

func (pub *Publisher) ApplicationCreated(event *events.ApplicationCreated) error {
	payloadJSON, err := json.Marshal(event)
	if err != nil {
		return err
	}

	err = pub.ch.Publish(
		pub.exchangeName,                    // exchange
		events.RoutingKeyApplicationCreated, // routing key
		false,                               // mandatory
		false,                               // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        payloadJSON,
		})

	return err
}
