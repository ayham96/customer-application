module gitlab.com/ayham96/customer_application

go 1.14

require (
	github.com/Netflix/go-env v0.0.0-20201103003909-014a952cefe2
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.0.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.8.0
	github.com/prometheus/common v0.15.0
	github.com/streadway/amqp v1.0.0
	google.golang.org/appengine v1.6.7 // indirect
)
