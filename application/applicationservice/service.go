package applicationservice

import (
	"database/sql"
	"errors"

	"github.com/google/uuid"
	"github.com/prometheus/common/log"
	"gitlab.com/ayham96/customer_application/application"
	"gitlab.com/ayham96/customer_application/application/applicationclient"
	"gitlab.com/ayham96/customer_application/events"
)

var ErrNotFound = errors.New("Application not found")

type ApplicationService struct {
	applicationclient application.ApplicationClient
	repo              application.ApplicationRepo
	publisher         application.ApplicationPublisher
}

func New(applicationclient application.ApplicationClient, repo application.ApplicationRepo, pub application.ApplicationPublisher) *ApplicationService {
	return &ApplicationService{
		applicationclient: applicationclient,
		repo:              repo,
		publisher:         pub,
	}
}

func (svc *ApplicationService) GetByID(id string) (*application.Application, error) {
	app, err := svc.repo.GetByID(id)
	if err == sql.ErrNoRows {
		return nil, ErrNotFound
	}
	return app, err
}

func (svc *ApplicationService) ListByStatus(status string) ([]*application.Application, error) {
	apps, err := svc.repo.ListByStatus(status)
	if err != nil {
		log.Warnf("failed to list applications by status: %s", err)
		return nil, err
	}

	return apps, nil
}

func (svc *ApplicationService) Create(input *application.ApplicationInput) (*application.Application, error) {
	appResp, err := svc.applicationclient.Create(&applicationclient.ApplicationPayload{
		ID:        uuid.New().String(),
		FirstName: input.FirstName,
		LastName:  input.LastName,
	})
	if err != nil {
		log.Warnf("failed to create application: %s", err)
		return nil, err
	}

	app, err := svc.repo.Create(&application.Application{
		ID:        appResp.ID,
		FirstName: appResp.FirstName,
		LastName:  appResp.LastName,
		Status:    application.ApplicationStatusPending,
	})
	if err != nil {
		log.Warnf("failed to store application: %s", err)
		return nil, err
	}
	err = svc.publisher.ApplicationCreated(&events.ApplicationCreated{
		ApplicationID: app.ID,
		FirstName:     app.FirstName,
		LastName:      app.LastName,
		Status:        app.Status,
	})
	if err != nil {
		log.Warnf("failed to publish application created event: %s", err)
	}

	return app, err
}

func (svc *ApplicationService) UpdateStatus(applicationID, newStatus string) error {
	err := svc.repo.UpdateStatus(applicationID, newStatus)
	if err != nil {
		log.Warnf("failed to update application status: %s", err)
		return err
	}

	return nil
}

func (svc *ApplicationService) HandleApplicationStatusUpdated(event *events.ApplicationStatusUpdated) error {
	return svc.UpdateStatus(event.ApplicationID, event.Status)
}
