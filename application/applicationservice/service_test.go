package applicationservice

import (
	"database/sql"
	"errors"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/ayham96/customer_application/application"
	"gitlab.com/ayham96/customer_application/application/applicationclient"
	"gitlab.com/ayham96/customer_application/events"
	"gitlab.com/ayham96/customer_application/tests/mocks/mock_application"
)

var (
	testPendingApplication = application.Application{
		ID:        "test-id-pending",
		FirstName: "Foo",
		LastName:  "Bar",
		Status:    application.ApplicationStatusPending,
	}
	testCompletedApplication = application.Application{
		ID:        "test-id-completed",
		FirstName: "Baz",
		LastName:  "Qux",
		Status:    application.ApplicationStatusPending,
	}
	testPendingApplications = []*application.Application{
		&testPendingApplication,
	}
)

func TestApplicationService_GetByID(t *testing.T) {
	tests := []struct {
		name                string
		givenID             string
		givenRepoErr        error
		expectedApplication *application.Application
		expectedErr         error
	}{
		{
			name:                "calls repo with correct id",
			givenID:             testPendingApplication.ID,
			expectedApplication: &testPendingApplication,
		},
		{
			name:                "returns ErrNotFound if not found",
			givenID:             "dummy-test-id",
			givenRepoErr:        sql.ErrNoRows,
			expectedApplication: nil,
			expectedErr:         ErrNotFound,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_application.NewMockApplicationClient(ctrl)
			publisher := mock_application.NewMockApplicationPublisher(ctrl)
			repo := mock_application.NewMockApplicationRepo(ctrl)
			repo.
				EXPECT().
				GetByID(gomock.Eq(tt.givenID)).
				Return(tt.expectedApplication, tt.givenRepoErr).
				Times(1)

			svc := New(client, repo, publisher)

			// Act
			got, err := svc.GetByID(tt.givenID)

			// Assert
			if err != tt.expectedErr {
				t.Errorf("ApplicationService.GetByID() error = %v, wantErr %v", err, tt.expectedErr)
				return
			}
			if !reflect.DeepEqual(got, tt.expectedApplication) {
				t.Errorf("ApplicationService.GetByID() = %v, want %v", got, tt.expectedApplication)
			}
		})
	}
}

func TestApplicationService_ListByStatus(t *testing.T) {
	status := application.ApplicationStatusPending
	applications := testPendingApplications

	tests := []struct {
		name       string
		repoReturn []*application.Application
		repoErr    error
		want       []*application.Application
		wantErr    bool
	}{
		{
			name:       "returns applications with the given status",
			repoReturn: applications,
			repoErr:    nil,
			want:       applications,
			wantErr:    false,
		},
		{
			name:       "returns error on failure",
			repoReturn: nil,
			repoErr:    sql.ErrConnDone,
			want:       nil,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_application.NewMockApplicationClient(ctrl)
			publisher := mock_application.NewMockApplicationPublisher(ctrl)
			repo := mock_application.NewMockApplicationRepo(ctrl)

			repo.
				EXPECT().
				ListByStatus(gomock.Eq(status)).
				Return(tt.repoReturn, tt.repoErr).
				Times(1)

			svc := New(client, repo, publisher)

			// Act
			got, err := svc.ListByStatus(status)

			// Assert
			if (err != nil) != tt.wantErr {
				t.Errorf("ApplicationService.ListByStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ApplicationService.ListByStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestApplicationService_Create(t *testing.T) {
	tests := []struct {
		name                       string
		clientErr                  error
		repoErr                    error
		publisherErr               error
		expectedClientCallTimes    int
		expectedRepoCallTimes      int
		expectedPublisherCallTimes int
		wantErr                    bool
	}{
		{
			name:                       "create application if all is successful",
			expectedClientCallTimes:    1,
			expectedRepoCallTimes:      1,
			expectedPublisherCallTimes: 1,
			wantErr:                    false,
		},
		{
			name:                       "publisher error",
			publisherErr:               errors.New("failed to publish"),
			expectedClientCallTimes:    1,
			expectedRepoCallTimes:      1,
			expectedPublisherCallTimes: 1,
			wantErr:                    true,
		},
		{
			name:                       "repo error",
			repoErr:                    errors.New("failed to store"),
			expectedClientCallTimes:    1,
			expectedRepoCallTimes:      1,
			expectedPublisherCallTimes: 0,
			wantErr:                    true,
		},
		{
			name:                       "api error",
			clientErr:                  errors.New("failed to request api"),
			expectedClientCallTimes:    1,
			expectedRepoCallTimes:      0,
			expectedPublisherCallTimes: 0,
			wantErr:                    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			expectedApplication := application.Application{
				FirstName: "Foo",
				LastName:  "Bar",
				Status:    application.ApplicationStatusPending,
			}

			client := mock_application.NewMockApplicationClient(ctrl)
			client.EXPECT().
				Create(gomock.Any()).
				DoAndReturn(func(payload *applicationclient.ApplicationPayload) (*applicationclient.ApplicationResponse, error) {
					if payload.ID == "" {
						t.Errorf("payload application ID is empty")
					}
					expectedApplication.ID = payload.ID

					if payload.FirstName != expectedApplication.FirstName {
						t.Errorf("payload first name doesn't match: expected %s got %s", expectedApplication.FirstName, payload.FirstName)
					}
					if payload.LastName != expectedApplication.LastName {
						t.Errorf("payload last name doesn't match: expected %s got %s", expectedApplication.LastName, payload.LastName)
					}

					if tt.clientErr != nil {
						return nil, tt.clientErr
					}

					return &applicationclient.ApplicationResponse{
						ID:        payload.ID,
						FirstName: payload.FirstName,
						LastName:  payload.LastName,
					}, nil
				}).
				Times(tt.expectedClientCallTimes)

			repo := mock_application.NewMockApplicationRepo(ctrl)
			repo.
				EXPECT().
				Create(gomock.Any()).
				DoAndReturn(func(in *application.Application) (*application.Application, error) {
					if in.ID != expectedApplication.ID {
						t.Errorf("repo got wrong application ID")
					}
					if in.FirstName != expectedApplication.FirstName {
						t.Errorf("repo first name doesn't match: expected %s got %s", expectedApplication.FirstName, in.FirstName)
					}
					if in.LastName != expectedApplication.LastName {
						t.Errorf("repo last name doesn't match: expected %s got %s", expectedApplication.LastName, in.LastName)
					}

					if tt.repoErr != nil {
						return nil, tt.repoErr
					}

					return in, nil
				}).
				Times(tt.expectedRepoCallTimes)

			publisher := mock_application.NewMockApplicationPublisher(ctrl)
			publisher.
				EXPECT().
				ApplicationCreated(gomock.Any()).
				DoAndReturn(func(event *events.ApplicationCreated) error {
					if event.ApplicationID != expectedApplication.ID {
						t.Errorf("publisher got wrong application ID")
					}
					if event.FirstName != expectedApplication.FirstName {
						t.Errorf("repo first name doesn't match: expected %s got %s", expectedApplication.FirstName, event.FirstName)
					}
					if event.LastName != expectedApplication.LastName {
						t.Errorf("repo last name doesn't match: expected %s got %s", expectedApplication.LastName, event.LastName)
					}

					return tt.publisherErr
				}).
				Times(tt.expectedPublisherCallTimes)

			svc := New(client, repo, publisher)

			// Act
			got, err := svc.Create(&application.ApplicationInput{
				FirstName: expectedApplication.FirstName,
				LastName:  expectedApplication.LastName,
			})

			// Assert
			if (err != nil) != tt.wantErr {
				t.Errorf("ApplicationService.Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && !reflect.DeepEqual(got, &expectedApplication) {
				t.Errorf("ApplicationService.Create() = %v, want %v", got, expectedApplication)
			}
		})
	}
}

func TestApplicationService_UpdateStatus(t *testing.T) {
	newStatus := application.ApplicationStatusCompleted

	tests := []struct {
		name    string
		repoErr error
		wantErr bool
	}{
		{
			name:    "updates application with no error",
			repoErr: nil,
			wantErr: false,
		},
		{
			name:    "returns error on failure",
			repoErr: sql.ErrConnDone,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_application.NewMockApplicationClient(ctrl)
			publisher := mock_application.NewMockApplicationPublisher(ctrl)
			repo := mock_application.NewMockApplicationRepo(ctrl)

			repo.
				EXPECT().
				UpdateStatus(gomock.Eq(testPendingApplication.ID), gomock.Eq(newStatus)).
				Return(tt.repoErr).
				Times(1)

			svc := New(client, repo, publisher)

			// Act
			err := svc.UpdateStatus(testPendingApplication.ID, newStatus)

			// Assert
			if (err != nil) != tt.wantErr {
				t.Errorf("ApplicationService.UpdateStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
