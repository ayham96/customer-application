package applicationrepo

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/ayham96/customer_application/application"
)

type ApplicationRepo struct {
	con                *sqlx.DB
	getByIDStmt        *sqlx.Stmt
	selectByStatusStmt *sqlx.Stmt
	insertStmt         *sqlx.NamedStmt
	updateStatusStmt   *sqlx.NamedStmt
}

func New(con *sqlx.DB) (*ApplicationRepo, error) {
	repo := &ApplicationRepo{
		con: con,
	}

	var err error
	repo.getByIDStmt, err = repo.con.Preparex(`
		SELECT id, first_name, last_name, status
		FROM loan_applications
		WHERE id=$1
	`)
	if err != nil {
		return nil, err
	}

	repo.selectByStatusStmt, err = repo.con.Preparex(`
		SELECT id, first_name, last_name, status
		FROM loan_applications
		WHERE status=$1
	`)
	if err != nil {
		return nil, err
	}

	repo.insertStmt, err = repo.con.PrepareNamed(`
		INSERT INTO loan_applications
		(id, first_name, last_name, status)
		VALUES (:id, :first_name, :last_name, :status)
		RETURNING id, first_name, last_name, status
	`)
	if err != nil {
		return nil, err
	}

	repo.updateStatusStmt, err = repo.con.PrepareNamed(`
		UPDATE loan_applications
		SET status=:status
		WHERE id=:id
	`)
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *ApplicationRepo) GetByID(id string) (*application.Application, error) {
	application := application.Application{}
	err := repo.getByIDStmt.Get(&application, id)
	if err != nil {
		return nil, err
	}
	return &application, nil
}

func (repo *ApplicationRepo) ListByStatus(status string) ([]*application.Application, error) {
	applications := []*application.Application{}
	err := repo.selectByStatusStmt.Select(&applications, status)
	if err != nil {
		return nil, err
	}
	return applications, nil
}

func (repo *ApplicationRepo) Create(input *application.Application) (*application.Application, error) {
	application := application.Application{}
	row := repo.insertStmt.QueryRowx(input)
	err := row.StructScan(&application)
	if err != nil {
		return nil, err
	}
	return &application, nil
}

func (repo *ApplicationRepo) UpdateStatus(id, status string) error {
	res, err := repo.updateStatusStmt.Exec(map[string]interface{}{
		"id":     id,
		"status": status,
	})
	if err != nil {
		return err
	}
	if n, _ := res.RowsAffected(); n != 1 {
		return fmt.Errorf("failed to update application status: expected 1 effected row got %d", n)
	}

	return err
}

func (repo *ApplicationRepo) Close() {
	repo.getByIDStmt.Close()
	repo.selectByStatusStmt.Close()
	repo.updateStatusStmt.Close()
	repo.insertStmt.Close()
}
