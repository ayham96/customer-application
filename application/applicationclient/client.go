package applicationclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type ApplicationPayload struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type ApplicationResponse struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type ApplicationClient struct {
	baseURL string
}

func New(baseURL string) *ApplicationClient {
	return &ApplicationClient{
		baseURL: baseURL,
	}
}

func (api *ApplicationClient) Create(payload *ApplicationPayload) (*ApplicationResponse, error) {
	jsonPayload, err := json.Marshal(payload)
	req, err := http.NewRequest("POST", api.baseURL, bytes.NewBuffer(jsonPayload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	if resp.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("failed to create application got http status %d expected %d", resp.StatusCode, http.StatusCreated)
	}

	var appResp ApplicationResponse
	err = json.Unmarshal(body, &appResp)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	return &appResp, err
}
