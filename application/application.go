package application

import (
	"gitlab.com/ayham96/customer_application/application/applicationclient"
	"gitlab.com/ayham96/customer_application/events"
)

const (
	ApplicationStatusPending   = "pending"
	ApplicationStatusCompleted = "completed"
	ApplicationStatusRejected  = "rejected"
	ApplicationStatusTimeout   = "timeout"
)

type ApplicationClient interface {
	Create(payload *applicationclient.ApplicationPayload) (*applicationclient.ApplicationResponse, error)
}

type ApplicationRepo interface {
	GetByID(id string) (*Application, error)
	ListByStatus(status string) ([]*Application, error)
	Create(input *Application) (*Application, error)
	UpdateStatus(id, status string) error
}

type ApplicationPublisher interface {
	ApplicationCreated(event *events.ApplicationCreated) error
}

type Application struct {
	ID        string `json:"id" db:"id"`
	FirstName string `json:"first_name" db:"first_name"`
	LastName  string `json:"last_name" db:"last_name"`
	Status    string `json:"status" db:"status"`
}

type ApplicationInput struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}
